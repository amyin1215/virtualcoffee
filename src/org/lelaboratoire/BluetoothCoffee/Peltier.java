package org.lelaboratoire.BluetoothCoffee;

import java.util.Timer;
import java.util.TimerTask;
import android.util.Log;

/* There needs to be one shared timer for all the peltiers */
public class Peltier {
	private static final boolean D = true;
	boolean hotState = false; // if hot
	boolean coldState = false; // if cold
	Fan[] fanList;
	private static final String TAG = "Peltier Class";
	public boolean scentOn = false; // if "scent is being experienced" aka 2 seconds after cold starts
	int peltierNum;
	OPhoneClass oPhone;
	
	public Peltier(int peltierNum, OPhoneClass oPhone){
		this.peltierNum = peltierNum;
		this.oPhone = oPhone;
	}

	boolean goHot() {
		if (D) Log.e(TAG, "goHot called");
		hotState = true;
		coldState = false;
    	scentOn = true;
		return oPhone.sendMessage((byte)(peltierNum*16+128 + OPhoneClass.HOT));		
	}
	
	boolean goCold() {
		hotState = false;
		coldState = true;
		return oPhone.sendMessage((byte)(peltierNum*16+128 + OPhoneClass.COLD));	
	}
	
	/* turns off blower while simultaneously turning on cold
	   After delay_cold, turns off lower fan and peltier */
	public void turnOff() {
		hotState = false;
		coldState = false;
		scentOn = false;
		oPhone.sendMessage((byte)(peltierNum*16+128 + OPhoneClass.OFF));
	}

}