package org.lelaboratoire.BluetoothCoffee;

import android.util.Log;

public class Fan {
	public static int SPEEDMAX = 7;
	public static int SPEEDMIN = 0;
	public boolean isOn = false;
	private static final String TAG = "Fan Class";
	private int fanNum;
	public static int FANCOUNT = 2;
	OPhoneClass oPhone;
	
	public Fan(int fanNum, OPhoneClass oPhone) {
		fanNum = fanNum;
		this.oPhone = oPhone;
	}
	
	public boolean turnOn(int speed) {
		if (speed > SPEEDMAX || speed < SPEEDMIN)
			return false;
		isOn = true;
		int fanValue;
		if (fanNum<FANCOUNT) {
			// Each fan after FAN0 needs additional 16
			fanValue = fanNum*16;
		} else {
			// the fan is out of index
			return false;
		}
		
		return oPhone.sendMessage((byte)(OPhoneClass.FAN0 + fanValue + OPhoneClass.SPEED + speed));
	}
	
	public void turnOff() {
		isOn = false;
		int fanValue;
		if (fanNum<FANCOUNT) {
			// Each fan after FAN0 needs additional 16
			fanValue = fanNum*16;
		} else {
			// the fan is out of index
			return;
		}
		oPhone.sendMessage((byte)(OPhoneClass.FAN0 + fanValue + OPhoneClass.OFF));
		Log.e(TAG, "Fan " + fanNum + " is off");
	}
}
