package org.lelaboratoire.BluetoothCoffee;

import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import android.content.Context;
import android.util.Log;

/**
 * Controls holistic OPhone functions
 * (Interface for protocol)
 * Use PIN 1234 to pair with OPhone initially
 * @author Amy Yin
 *
 */
public final class OPhoneClass {
	public static Peltier[] peltierList;
	public static boolean setup = false;
	private static Fan[] fanList;
	public static boolean peltierActivated = false;
	BTClass bt;
	
	/* Commands */
	public final static int ON = 1;
	public final static int OFF = 2;
	public final static int HOT = 3;
	public final static int COLD = 4;
	public final static int STATUS = 5;
	public final static int ERROR = 6;
	public final static int SPEED = 8;
	
	/* Components */
	public final static int OPHONE = 16;
	public final static int ANDROID = 32;
	public final static int LED = 48;
	public final static int FAN0 = 64;
	public final static int FAN1 = 80;

	public final int PELTIER0 = 128;
	public final int PELTIER1 = 144;
	public final int PELTIER2 = 160;
	public final int PELTIER3 = 176;
	
	public final static int SCENTSCOUNT = 4;
	public final static int FANMAX = 2;
	private final static int BLOWER = 1;
	private final static int BOXFAN = 0;
	private final static long COLDTIME = 4000; // after scentOff is called, duration that Peltier is cold
	
	
	private static final String TAG = "OPhoneClass";
	private static ScheduledExecutorService scheduleTaskExecutor;
	
	/* Public constructor
	 * @param bt Must use the same instance of BT that was used to initially to connect to OPhone
	 */
	public OPhoneClass(BTClass bt) {	

		this.bt = bt;
		peltierList = new Peltier[SCENTSCOUNT];
		for (int i=0; i<SCENTSCOUNT; i++) {
			peltierList[i] = new Peltier(i, this);
		}
		
		fanList = new Fan[FANMAX];
		for (int i=0; i<FANMAX; i++) {
			fanList[i] = new Fan(i, this);
		}
		setup = true;
		Log.e(TAG, "Setup");
	}
	
	/* Turns scent on
	 * @param duration How long scent should be experienced in milliseconds minus initial delay
	 * @param scent Which scent to activate; only one peltier can be active at a time, HOT or COLD
	 */
	public boolean scentOn(int scent, int duration) {
		scentOn(scent);
		scheduleScentOff(scent, duration);
		return true;
	}
	
	public boolean scentOn(int scent) {
		if (!setup) return false;
		if (scent < 0 || scent > SCENTSCOUNT)
			return false;

		// check that another peltier is not already on
		if (peltierActivated)
			return false;
		// Turn peltier hot and fan on
		peltierList[scent].goHot();
		fanList[BOXFAN].turnOn(Fan.SPEEDMAX);
		fanList[BLOWER].turnOn(Fan.SPEEDMAX);
		return true;
	}

	public void scheduleScentOff(int scent, int delay) {
		if (!setup) return;
		
		Log.e(TAG, "Schedule scent off with delay of " + delay);
		scheduleTaskExecutor= Executors.newScheduledThreadPool(5);
		// Switch Peltier from HOT to COLD
		scheduleTaskExecutor.schedule(new ColdTask(scent), Long.valueOf(delay), TimeUnit.MILLISECONDS);
		// Then turn peltier and fans off
		scheduleTaskExecutor.schedule(new OffTask(scent), Long.valueOf(delay)+COLDTIME, TimeUnit.MILLISECONDS);
	}
	
	public boolean checkStatus() {
		String s = "";
		bt.sendDataByte((byte)(OPHONE + STATUS));
		byte[] msg = null;
		if (null != (msg = bt.receiveData())) {
			for (int i = 0; i < msg.length; i++) {
				s += (char)msg[i];
			}
		}
		return (s != "");

	}
	public class ColdTask extends TimerTask {
		int scent;		
	    public ColdTask(int scent) {
	    	this.scent = scent;
	    }
        @Override
        public void run() {
        	Log.e(TAG, "ColdTask turning scent " + scent + " off");
        	peltierList[scent].goCold();
        }
   };
   
   public String switchLED(boolean on){
	   if (on) {
		   bt.sendData(Character.toString((char) 49));
	   } else {
		   bt.sendData(Character.toString((char) 50));
	   }
	   
		byte[] msg = null;
		if (null != (msg = bt.receiveData())) {
			String s = "";
			for (int i = 0; i < msg.length; i++) {
				s += (char)msg[i];
			}
			return s;
		}
	   return null;
   }
	
	public class OffTask extends TimerTask {
		int scent;
		
	     public OffTask(int scent) {
	         this.scent = scent;
	     }

        @Override
        public void run() {
        	Log.e(TAG, "OffTask turning scent " + scent + " off");
        	peltierList[scent].turnOff();
        	fanList[BOXFAN].turnOff();
        	fanList[BLOWER].turnOff();
        }
   };	
   
	/* Send command to Protocol
	 * If successful, OPhone will send back same message as confirmation
	 */
	public boolean sendMessage(byte message) {
		String s = "";
		bt.sendDataByte(message);
		byte[] msg = null;
		if (null != (msg = bt.receiveData())) {
			for (int i = 0; i < msg.length; i++) {
				s += (char)msg[i];
			}
		}

		// if same message not returned, ERROR in procedure
		Log.e(TAG, "OPhone's response to <<" + message+ ">> is " + s);
		return (s == ""+message);

	}

	/*  Turns off all peltiers, fans, LED etc. */
	public String turnOffAllComponents() {
		String s = "";
		bt.sendDataByte((byte)(OPHONE+OFF));
		byte[] msg = null;
		if (null != (msg = bt.receiveData())) {
			for (int i = 0; i < msg.length; i++) {
				s += Integer.toBinaryString((int)msg[i]);
			}
		}
		return s;
	}

}
