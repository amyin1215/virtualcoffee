package org.lelaboratoire.virtualcoffee;

import java.io.IOException;

import org.lelaboratoire.BluetoothCoffee.OPhoneClass;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;

import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class ViewMessageFragment extends SherlockFragment {
	//private MusicService mServ;
	private static final String TAG = "Single Scent vCoffee";
	private int track = -1;
	private static int id;
	private String message;
	private String sender;
	private String name;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		Log.e(TAG, "onCreateView");
		id = getArguments().getInt("id",0);
		message = getArguments().getString("message");
		sender = getArguments().getString("sender");
		name = getArguments().getString("name");
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.single_scent, container, false);
		setup(view);
		return view;
	}
	
	public static ViewMessageFragment newInstance(int id, String sender, String message, String name) {
		ViewMessageFragment myFragment = new ViewMessageFragment ();
	    Bundle args = new Bundle();
	    args.putInt("id", id);
	    args.putString("sender", sender);
	    args.putString("message", message);
	    args.putString("name", name);
	    myFragment.setArguments(args);

	    return myFragment;
	}

	private void setup(View view) {
		ResourcesHolder.setScreen(getActivity().getBaseContext(), id, 
				(Button)view.findViewById(R.id.background), 
				(TextView)view.findViewById(R.id.label));
		TextView senderView = (TextView) view.findViewById(R.id.sender);
		TextView messageView = (TextView) view.findViewById(R.id.message);
		
		String escapedUsername = TextUtils.htmlEncode(name);
		Resources res = getResources();
		String text = String.format(res.getString(R.string.message), escapedUsername);
		CharSequence styledText = Html.fromHtml(text);
		
		senderView.setText(styledText);
		messageView.setText("\"" + message + "\"");
		
		Button background = (Button)view.findViewById(R.id.background);
		background.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				scentRelease(v);
			}
		});
	}

	public void scentRelease(View view) {
		// if track is on
		Log.e(TAG, "release");
		// if track is currently playing
		if (track >= 0 && track < 4) {
			MainActivity.mServ.pauseMusic();
			track = -1;
			((MainActivity)getActivity()).oPhone.scheduleScentOff(id,0);
			
		} else {
			Log.e(TAG, "resume music track: " + track + "; id= " + id);
			try {
				MainActivity.mServ.resumeMusic(id, getActivity());
			} catch (IllegalStateException e) {
				MainActivity.mServ.pauseMusic();
				e.printStackTrace();
				track = -1;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			track = id;
			((MainActivity)getActivity()).oPhone.scentOn(id);
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}
}
