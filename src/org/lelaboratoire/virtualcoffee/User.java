package org.lelaboratoire.virtualcoffee;

public class User{

    private int _id;
    private String _name;
    private String _email;

    public User(){
        this._id = 0;
        this._name = "";
    }

    public void setId(int id){
        this._id = id;
    }

    public int getId(){
        return this._id;
    }

    public void setName(String name){
        this._name = name;
    }

    public String getName(){
        return this._name;
    }
    
    public void setEmail(String email) {
    	this._email = email;
    }
    
    public String getEmail(){
    	return this._email;
    }
}