package org.lelaboratoire.virtualcoffee;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public final class ResourcesHolder {
	
	@SuppressLint("NewApi")
	public static void setScreen(Context context, int id, View view, TextView label) {
		Resources res = context.getResources();
		
		String scentName = res.getStringArray(R.array.scents_array)[id]; 
		TypedArray array = res.obtainTypedArray(R.array.drawable_array);
		Drawable scentColor = array.getDrawable(id);
		
		int sdk = android.os.Build.VERSION.SDK_INT;
		if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
		    view.setBackgroundDrawable(scentColor);
		} else {
		    view.setBackground(scentColor);
		}	
		
		if (label != null) 
			label.setText(scentName);
		view.setTag(""+id);
	}

}
