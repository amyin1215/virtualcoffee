package org.lelaboratoire.virtualcoffee;
 
import java.util.ArrayList;
import java.util.HashMap;
 
import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
 
public class InboxAdapter extends BaseAdapter {
 
    private Activity activity;
    public ArrayList<HashMap<String, String>> data;
    private static LayoutInflater inflater=null;
    private long id;
 
    public InboxAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
        activity = a;
        data=d;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
 
    public int getCount() {
        return data.size();
    }
 
    public Object getItem(int position) {
        return data.get(position);
    }
 
    public long getItemId(int position) {
        return id;
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.list_row, null);
 
        TextView sender = (TextView)vi.findViewById(R.id.sender); // sender
        TextView message_text = (TextView)vi.findViewById(R.id.message); // message body
        TextView date = (TextView)vi.findViewById(R.id.date); // timestamp
        ImageView thumb_image=(ImageView)vi.findViewById(R.id.list_image); // scent color image
 
        HashMap<String, String> message = new HashMap<String, String>();
        message = data.get(position);
 
        // Setting all values in listview
        sender.setText(message.get(InboxFragment.KEY_SENDER_NAME));
        message_text.setText(message.get(InboxFragment.KEY_MESSAGE));
        date.setText(message.get(InboxFragment.KEY_DATE));
        
        Resources res = activity.getResources();
		TypedArray array = res.obtainTypedArray(R.array.drawable_array);
		int scentid = Integer.parseInt(message.get(InboxFragment.KEY_SCENT));
		Drawable scentColor = array.getDrawable(scentid);      
		thumb_image.setImageDrawable(scentColor);
		
		return vi;
    }
}