package org.lelaboratoire.virtualcoffee;

import com.actionbarsherlock.app.SherlockFragment;
import com.google.android.gcm.GCMRegistrar;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
 
public class AccountFragment extends SherlockFragment {
    // alert dialog manager
    AlertDialogManager alert = new AlertDialogManager();
 
    // Internet detector
    ConnectionDetector cd;
 
    // UI elements
    EditText txtName;
    EditText txtEmail;
 
    // Register button
    Button btnRegister;
    
	class GetAccountInfoTask extends AsyncTask<String, User, User> {
	    @Override
	    protected User doInBackground(String... params) {
	    	return CoffeeServer.getAccountInfo(params[0], getActivity());
	    }  
	    @Override
	    protected void onPostExecute(User user) {
	    	txtName = (EditText) getActivity().findViewById(R.id.txtName);
	    	txtEmail = (EditText) getActivity().findViewById(R.id.txtEmail);
	    	if (user == null) {
		    	txtName.setText("Unable to connect to Virtual Coffee Server");    	
		    	txtEmail.setText("Unable to connect to Virtual Coffee Server");
	    		alert.showAlertDialog(getActivity(),
	                    "Server Error",
	                    "Cannot Connect to Virtual Coffee Server. Please check your Wifi connection!", false);
	    		Log.e("onPostExecute", "user null");
	    	} else {
	    		Log.e("onPostExecute", "user not null");
		    	txtName.setText(user.getName());    	
		    	txtEmail.setText(user.getEmail());
	    	}
	    }
	}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
	
    	View view = inflater.inflate(R.layout.fragment_account, container, false);
        txtName = (EditText) view.findViewById(R.id.txtName);
        txtEmail = (EditText) view.findViewById(R.id.txtEmail);
        btnRegister = (Button) view.findViewById(R.id.btnRegister);
        
    	if (GCMRegistrar.isRegisteredOnServer(getActivity())) {
    		btnRegister.setText("Already Registered");
    		btnRegister.setEnabled(false);
    		String regId = GCMRegistrar.getRegistrationId(getActivity());
    		new GetAccountInfoTask().execute(regId);
    		
    	} else {
	        cd = new ConnectionDetector(getActivity().getApplicationContext());
	 
	        // Check if Internet present
	        if (!cd.isConnectingToInternet()) {
	            // Internet Connection is not present
	            alert.showAlertDialog(getActivity(),
	                    "Internet Connection Error",
	                    "Please connect to working Internet connection", false);
	            // stop executing code by return
	            return view;
	        }
	 
	        /*
	         * Click event on Register button
	         * */
	        btnRegister.setOnClickListener(new View.OnClickListener() {
	 
	            @Override
	            public void onClick(View arg0) {
	                // Read EditText dat
	                String name = txtName.getText().toString();
	                String email = txtEmail.getText().toString();
	 
	                // Check if user filled the form
	                if(name.trim().length() > 0 && email.trim().length() > 0){
	                    // Launch Main Activity
	                    Intent i = new Intent(getActivity().getApplicationContext(), MainActivity.class);
	                    i.addCategory("Register");
	                    // Registering user on our server
	                    // Sending registration details to MainActivity
	                    i.putExtra("name", name);
	                    i.putExtra("email", email);
	                    startActivity(i);
	                }else{
	                    // user didn't filled in data
	                    // request form to be filled in
	                    alert.showAlertDialog(getActivity(), "Registration Error!", "Please enter your details", false);
	                }
	            }
	        });
    	}
        return view;
    } 
}