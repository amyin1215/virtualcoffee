package org.lelaboratoire.virtualcoffee;

import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.actionbarsherlock.app.ActionBar;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

public class TabsListener<T extends SherlockFragment> implements ActionBar.TabListener {
    private SherlockFragment mFragment;
    private final SherlockFragmentActivity mActivity;
    private final String mTag;
    private final Class<T> mClass;
    private final String TAG = "TabsListener";

    /** Constructor used each time a new tab is created.
      * @param activity  The host Activity, used to instantiate the fragment
      * @param tag  The identifier tag for the fragment
      * @param clz  The fragment's Class, used to instantiate the fragment
      */
    public TabsListener(SherlockFragmentActivity activity, String tag, Class<T> clz) {
        mActivity = activity;
        mTag = tag;
        mClass = clz;
    }

    /* The following are each of the ActionBar.TabListener callbacks */

    public void onTabSelected(Tab tab, FragmentTransaction ft) {
        // check if on ViewMessage Fragment
        SherlockFragment preInitializedFragment = (SherlockFragment) mActivity.getSupportFragmentManager().findFragmentByTag("ViewMessage");
        if (preInitializedFragment != null) {
        	Log.e(TAG, "preInitialized != null");
        	// get rid of ViewMessage  if it exists
            ft.detach(preInitializedFragment);
            // clear backstack so doesn't go back to inbox
            FragmentManager fm = mActivity.getSupportFragmentManager();
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        } 
        
     // Check if the fragment is already initialized
        preInitializedFragment = (SherlockFragment) mActivity.getSupportFragmentManager().findFragmentByTag(mTag);
        if (preInitializedFragment == null) {
            mFragment = (SherlockFragment) SherlockFragment.instantiate(mActivity, mClass.getName());
            ft.add(android.R.id.content, mFragment, mTag);
        } else {
        	// If it exists, simply attach it in order to show it
            ft.attach(preInitializedFragment);
        }

    }

    public void onTabUnselected(Tab tab, FragmentTransaction ft) {   
        SherlockFragment preInitializedFragment = (SherlockFragment) mActivity.getSupportFragmentManager().findFragmentByTag(mTag);

        if (preInitializedFragment != null) {
        	Log.e("TabsListener", "onTabUnselected: preInitiatalized not null so detach " + mTag);
            ft.detach(preInitializedFragment);
        } else if (mFragment != null) {
        	Log.e(TAG, "onTabUnselected: ft.detach " + mFragment);
            ft.detach(mFragment);
        }
    }

    public void onTabReselected(Tab tab, FragmentTransaction ft) {
    	if (tab.getText() == "Inbox") {
    		// refresh inbox, or go back to inbox if in ViewMessage
    		Log.e("TabsListener", "ontabReselected Inbox");
            SherlockFragment preInitializedFragment = (SherlockFragment) mActivity.getSupportFragmentManager().findFragmentByTag(mTag);

            if (preInitializedFragment != null) {
            	Log.e(TAG, "preInitializedFragment != null");
                ft.detach(preInitializedFragment);
            } else if (mFragment != null) {
                ft.detach(mFragment);
            }
            
            // check if on ViewMessage Fragment
            preInitializedFragment = (SherlockFragment) mActivity.getSupportFragmentManager().findFragmentByTag("ViewMessage");
            if (preInitializedFragment != null) {
                ft.detach(preInitializedFragment);
            } 
            
            mFragment = (SherlockFragment) SherlockFragment.instantiate(mActivity, mClass.getName());
            ft.add(android.R.id.content, mFragment, mTag);

    	}
    }
}