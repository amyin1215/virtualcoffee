package org.lelaboratoire.virtualcoffee;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.actionbarsherlock.app.SherlockFragment;
import com.google.android.gcm.GCMRegistrar;

import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract.Data;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class InboxFragment extends SherlockFragment{
	private static final String TAG = "Fragment Inbox Virtual Coffee";
    View view;
    static final String KEY_SONG = "song"; // parent node
    static final String KEY_ID = "id";
    static final String KEY_SENDER = "Sender Id";
    static final String KEY_SENDER_NAME = "Sender Name";
    static final String KEY_MESSAGE = "Message";
    static final String KEY_DATE = "Date";
    static final String KEY_SCENT = "1";
 
    ListView list;
    InboxAdapter adapter;
    ArrayList<HashMap<String, String>> messagesList;

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		Log.e(TAG, "onCreateView");
		// Inflate the layout for this fragment
		view = inflater.inflate(R.layout.inbox_list, container, false);
		new GetMessagesTask().execute("");
		return view;
	}

    private void addMessage(String id, String message, 
    		String sender, String senderName, String scentid, String date) {
    	
        // creating new HashMap
        HashMap<String, String> map = new HashMap<String, String>();
        // adding each child node to HashMap key => value
        map.put(KEY_ID, id);
        map.put(KEY_SENDER, sender);
        map.put(KEY_SENDER_NAME, senderName);
        map.put(KEY_MESSAGE, message);
        map.put(KEY_DATE, date);
        map.put(KEY_SCENT, scentid);

        // adding HashList to ArrayList
        messagesList.add(map);	
    }
    
    private void setAdapter() {
        list=(ListView)view.findViewById(R.id.list);
 
        // Getting adapter by passing xml data ArrayList
        adapter=new InboxAdapter(getActivity(), messagesList);
        list.setAdapter(adapter);
 
        // Click event for single list row
        list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int pos, long id) {
            	// get message info
            	HashMap<String, String> message = (HashMap<String, String>) adapter.getItem(pos);
            	int scentid = Integer.parseInt(message.get(KEY_SCENT));
            	String sender = message.get(KEY_SENDER);
            	String text = message.get(KEY_MESSAGE);
            	String name = message.get(KEY_SENDER_NAME);
            	((MainActivity) getActivity()).viewMessage(scentid, sender, text, name);
            }
        });
    }
	class GetMessagesTask extends AsyncTask<String, Void, JSONArray> {
	    @Override
	    protected JSONArray doInBackground(String... params) {
			if (!GCMRegistrar.isRegistered(getActivity())) {
				return null; 
			}
	    	String regid = GCMRegistrar.getRegistrationId(getActivity());
	    	return CoffeeServer.getMessages(regid);
	    }  
	    @Override
	    protected void onPostExecute(JSONArray jArray) {
			messagesList = new ArrayList<HashMap<String, String>>();
			LinearLayout emptyView = (LinearLayout) view.findViewById(R.id.emptyView);
			TextView empty = (TextView) view.findViewById(R.id.emptyTextView);
			
			if (!GCMRegistrar.isRegistered(getActivity())) {
				empty.setText("Please register this device under the Account Tab");
				return; 
			}
			
			if (jArray == null) {
				empty.setText("No Messages");
			} else {
		    	for(int i=0;i<jArray.length();i++){
		        	JSONObject json_data;
					try {
						json_data = jArray.getJSONObject(i);
			        	addMessage(""+json_data.getInt("message_id"),
				            	json_data.getString("message"),
				            	""+json_data.getInt("sender_id"),
				            	json_data.getString("name"),
				            	""+json_data.getInt("scent_id"),
				            	json_data.getString("date")
				            );
			        	Log.e("log_tag","json_data: " + json_data.toString());
					} catch (JSONException e) {
						e.printStackTrace();
						empty.setText("Error parsing messages");
					}
		    	}
			}
	    	setAdapter();			

			ListView list = (ListView)view.findViewById(R.id.list);	
    		list.setEmptyView(emptyView);
	    }
	}

}
