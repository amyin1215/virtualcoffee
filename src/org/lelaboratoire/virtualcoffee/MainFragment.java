package org.lelaboratoire.virtualcoffee;

import com.actionbarsherlock.app.SherlockFragment;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;

public class MainFragment extends SherlockFragment implements OnClickListener {
	private static final String TAG = "Fragment Main Virtual Coffee";
	public int track = -1; // 0 - 4 scents; -1 is off
	private ImageView mail0;
	private ImageView mail1;
	private ImageView mail2;
	private ImageView mail3;
    View view;

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		Log.e(TAG, "onCreateView");
		// Inflate the layout for this fragment
		view = inflater.inflate(R.layout.activity_main, container, false);
		mailSetup();
		return view;
	}
	
	private void mailSetup() {
		mail0 = (ImageView) view.findViewById(R.id.mail0);
		mail1 = (ImageView) view.findViewById(R.id.mail1);
		mail2 = (ImageView) view.findViewById(R.id.mail2);
		mail3 = (ImageView) view.findViewById(R.id.mail3);
	    mail0.setOnClickListener(this);
	    mail1.setOnClickListener(this);
	    mail2.setOnClickListener(this);
	    mail3.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		((MainActivity) getActivity()).composeOMessage(Integer.valueOf(v.getTag().toString()));
		
	}
	
}
